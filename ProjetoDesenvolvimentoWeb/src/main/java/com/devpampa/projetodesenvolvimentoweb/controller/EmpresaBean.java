/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.dao.DaoEmpresa;
import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "empresaBean") //define como a view vai poder usar ela, chamando usuarioBean.
@ViewScoped 
public class EmpresaBean implements Serializable {

    @EJB // notação do Enterprise JavaBeans (EJB) que é um componente da plataforma JEE que roda em um container de um servidor de aplicação. https://pt.wikipedia.org/wiki/Enterprise_JavaBeans
    DaoEmpresa daoEmpresa; // Injeção da classe daoUsuario no container. Como se estivesse dando um "new" nessa instância
    private Empresa empresaNova;
    private boolean detalhe,editar,novo;

    public EmpresaBean() {
        System.out.println("Construtor sendo executado");
        empresaNova = (Empresa) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("empresaselecionada"); //recupera usuario do escopo
        
        if (empresaNova == null) {
            empresaNova = new Empresa();
            novo = true;
            detalhe = false;
            editar = false;
        } else {
            if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalhes_empresa") != null) {
                detalhe = (boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalhes_empresa"); //recupera usuario do escopo
            
            } else if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editar_empresa") != null) {
                 editar = (boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editar_empresa");
            }

        }

        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("empresaselecionada"); //recupera usuario do escopo
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("editar_empresa"); //recupera usuario do escopo  
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("detalhes_empresa"); //recupera usuario do escopo

    }


    public void salvarEmpresa() {
        System.out.println("Entrou no salvar empresa");

        if (empresaNova != null) {
            if (daoEmpresa.updateEmpresa(empresaNova) != null) {
                empresaNova = new Empresa();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Empresa Salva com Sucesso!"));
              
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Falha, problema ao Salvar!"));
            }
        } else {
            System.out.println("Problemas ao cadastrar");
        }
    }
    
     public String identificarPerfil(Empresa empresaSelecinada) {
        if (empresaSelecinada != null) {
            if (null == empresaSelecinada.getStatus()) {
                return "Ativa";
            } else {
                switch (empresaSelecinada.getStatus()) {
                    case 1:
                        return "Ativa";
                    case 2:
                        return "Inativa";
                    default:
                        return "Ativa";
                }
            }
        }
        return "-";
    }

    public void atualizarEmpresa() {
        empresaNova = daoEmpresa.update(empresaNova);
    }

    public DaoEmpresa getDaoEmpresa() {
        return daoEmpresa;
    }

    public void setDaoEmpresa(DaoEmpresa daoEmpresa) {
        this.daoEmpresa = daoEmpresa;
    }

    public Empresa getEmpresaNova() {
        return empresaNova;
    }

    public void setEmpresaNova(Empresa empresaNova) {
        this.empresaNova = empresaNova;
    }

    public boolean isDetalhe() {
        return detalhe;
    }

    public void setDetalhe(boolean detalhe) {
        this.detalhe = detalhe;
    }

    public boolean isEditar() {
        return editar;
    }

    public void setEditar(boolean editar) {
        this.editar = editar;
    }

    public boolean isNovo() {
        return novo;
    }

    public void setNovo(boolean novo) {
        this.novo = novo;
    }

    
}
