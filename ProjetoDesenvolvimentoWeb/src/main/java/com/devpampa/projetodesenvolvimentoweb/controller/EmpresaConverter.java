/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import java.util.List;
import java.util.Objects;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Peterson
 */
@FacesConverter(value = "empresaConverter")
public class EmpresaConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent uiComponent, String beerId) {
        ValueExpression vex
                = ctx.getApplication().getExpressionFactory()
                        .createValueExpression(ctx.getELContext(),
                                "#{empresasBean}", EmpresasBean.class);

        EmpresasBean beers = (EmpresasBean) vex.getValue(ctx.getELContext());

        return this.buscarId(Integer.valueOf(beerId), beers.getListaEmpresa());
    }

    private Empresa buscarId(Integer id, List<Empresa> arbitros) {
        if (id != null && arbitros != null) {
            for (Empresa arbitro : arbitros) {
                if (Objects.equals(arbitro.getIdempresa(), id)) {
                    return arbitro;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object beer) {
        if (beer != null) {
            return ((Empresa) beer).getIdempresa().toString();
        } else {
            return null;
        }
    }

}
