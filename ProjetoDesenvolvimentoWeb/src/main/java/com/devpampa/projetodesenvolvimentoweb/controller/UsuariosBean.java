/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.dao.DaoUsuario;
import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "usuariosBean") 
@ViewScoped 
public class UsuariosBean implements Serializable {
    //usar uma ejb para cada DAO
    @EJB // notação do Enterprise JavaBeans (EJB) que é um componente da plataforma JEE que roda em um container de um servidor de aplicação. https://pt.wikipedia.org/wiki/Enterprise_JavaBeans
    DaoUsuario daoUsuario; // Injeção da classe daoUsuario no container. Como se estivesse dando um "new" nessa instância
    
    private List<Usuario> listaUsuarios; //lista para pegar todos usuarios do banco

    public UsuariosBean() {
        System.out.println("Construtor sendo executado");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Lista de usuarios"));
    }

    @PostConstruct
    public void init() {
        System.out.println("Init sendo executado");
        listaUsuarios = daoUsuario.buscarUsuarios();
    }


    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
    
    public void verDetalhes(Usuario usuarioSelecionado){
        System.out.println("Selecionou verDetalhes");
        // Aqui vcs pode enviar o objeto usuário para outra tela captar. Esse usuário será adicionado a session através do comando:
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("usuarioselecionado", usuarioSelecionado); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("detalheusuario", true); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("editarusuario", false); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("formularioUsuario.xhtml"); // redirecionado para uma página
        } catch (IOException ex) {
            Logger.getLogger(UsuariosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void editarUsuario(Usuario usuarioSelecionado) {
        System.out.println("Selecionou editar");
        // Aqui vcs pode enviar o objeto usuário para outra tela captar. Esse usuário será adicionado a session através do comando:
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("usuarioselecionado", usuarioSelecionado); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("detalheusuario", false); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("editarusuario", true); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("formularioUsuario.xhtml"); // redirecionado para uma página
        } catch (IOException ex) {
            Logger.getLogger(UsuariosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void excluirUsuario(Usuario usuarioSelecionado){
       if (usuarioSelecionado != null) {
            boolean resultado = daoUsuario.excluirUsuario(usuarioSelecionado.getIdusuario());
            if (resultado) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Usuário deletado com sucesso!"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Problemas ao deletar o usuário!"));
            }
            listaUsuarios.remove(usuarioSelecionado);
        }
    }
     public String identificarUsuario(Usuario usuarioSelecionado){
        if(null == usuarioSelecionado.getPerfil()){
            return "Barbeiro";
        }else switch (usuarioSelecionado.getPerfil()) {
            case 1:
                return "Admin";
            case 0:
                return "Cabelereiro";
            default:
                return "Barbeiro";
        }
    }
     
    public void atualizarLista() {
        listaUsuarios = daoUsuario.buscarUsuarios();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Lista Atualizada!"));
    }

   
}
