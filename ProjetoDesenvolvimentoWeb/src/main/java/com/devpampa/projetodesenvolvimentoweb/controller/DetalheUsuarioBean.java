/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "detalheUsuarioBean")
@ViewScoped
public class DetalheUsuarioBean implements Serializable {

    private Usuario usuarioDetalhes;

    /**
     * Creates a new instance of DetalheUsuarioBean
     */
    public DetalheUsuarioBean() {

    }

    @PostConstruct
    public void init() {
        usuarioDetalhes = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("usuarioselecionado"); //recupera usuario do escopo
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("usuarioselecionado"); // remove usuario do escopo
    }

    public Usuario getUsuarioDetalhes() {
        return usuarioDetalhes;
    }

    public void setUsuarioDetalhes(Usuario usuarioDetalhes) {
        this.usuarioDetalhes = usuarioDetalhes;
    }

}
