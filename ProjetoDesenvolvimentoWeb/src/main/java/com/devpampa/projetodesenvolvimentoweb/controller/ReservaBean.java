/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.dao.DaoFilial;
import com.devpampa.projetodesenvolvimentoweb.dao.DaoReserva;
import com.devpampa.projetodesenvolvimentoweb.dao.DaoUsuario;
import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import com.devpampa.projetodesenvolvimentoweb.model.Filial;
import com.devpampa.projetodesenvolvimentoweb.model.Reserva;
import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author gomes
 */
@Named(value = "reservaBean")
@Dependent
public class ReservaBean {

    /**
     * Creates a new instance of FiliaisBean
     */
     @EJB // notação do Enterprise JavaBeans (EJB) que é um componente da plataforma JEE que roda em um container de um servidor de aplicação. https://pt.wikipedia.org/wiki/Enterprise_JavaBeans
    DaoReserva daoReserva;  
    private Reserva reservaNovo;
    private Usuario usuarioSelecionado;
    private Empresa empresaSelecionada;
    

    private boolean novo, detalhe, editar;

    public ReservaBean() {
        System.out.println("Construtor sendo executado");
        reservaNovo = (Reserva) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("reservaselecionado");
        if (reservaNovo == null) {
            novo = true;
            detalhe = false;
            editar = false;
            reservaNovo = new Reserva();
        } else {
            if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalhe_reserva") != null) {
                detalhe = (boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalhe_reserva");
            } else if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editar_reserva") != null) {
                editar = (boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editar_reserva");
            }
        }
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("reservaselecionado");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("detalhe_reserva");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("editar_reserva");
    }

    public void salvarFilial() {
        System.out.println("Entrou no salvar reserva");

        if (reservaNovo != null) {
            if (daoReserva.criarReserva(reservaNovo) != null) {
                reservaNovo = new Reserva();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Filial Salva com Sucesso!"));

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Problemas ao Salvar"));

            }
        } else {
            System.out.println("Problemas ao cadastrar");
        }
    }

    public String identificarStatus(Reserva reservaSelecionada) {
        if (reservaSelecionada != null) {
            if (null == reservaSelecionada.getStatus()) {
                return "Ativa";
            } else {
                switch (reservaSelecionada.getStatus()) {
                    case 1:
                        return "Pendente";
                    case 2:
                        return "Concluída";
                    default:
                        return "Cancelada";
                }
            }
        }
        return "-";
    }

    public DaoReserva getDaoReserva() {
        return daoReserva;
    }

    public void setDaoReserva(DaoReserva daoReserva) {
        this.daoReserva = daoReserva;
    }

    public Reserva getReservaNovo() {
        return reservaNovo;
    }

    public void setReservaNovo(Reserva reservaNovo) {
        this.reservaNovo = reservaNovo;
    }

    public Usuario getUsuarioSelecionado() {
        return usuarioSelecionado;
    }

    public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
        this.usuarioSelecionado = usuarioSelecionado;
    }

    public Empresa getEmpresaSelecionada() {
        return empresaSelecionada;
    }

    public void setEmpresaSelecionada(Empresa empresaSelecionada) {
        this.empresaSelecionada = empresaSelecionada;
    }

    public boolean isNovo() {
        return novo;
    }

    public void setNovo(boolean novo) {
        this.novo = novo;
    }

    public boolean isDetalhe() {
        return detalhe;
    }

    public void setDetalhe(boolean detalhe) {
        this.detalhe = detalhe;
    }

    public boolean isEditar() {
        return editar;
    }

    public void setEditar(boolean editar) {
        this.editar = editar;
    }

    
    


}
