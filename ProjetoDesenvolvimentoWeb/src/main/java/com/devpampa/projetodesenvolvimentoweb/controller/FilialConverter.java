/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.model.Filial;
import java.util.List;
import java.util.Objects;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Peterson
 */
@FacesConverter(value = "filialConverter")
public class FilialConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent uiComponent, String beerId) {
        ValueExpression vex
                = ctx.getApplication().getExpressionFactory()
                        .createValueExpression(ctx.getELContext(),
                                "#{filiaisBean}", FiliaisBean.class);

       FiliaisBean beers = (FiliaisBean) vex.getValue(ctx.getELContext());

        return this.buscarId(Integer.valueOf(beerId), beers.getListaFilial());
    }

    private Filial buscarId(Integer id, List<Filial> arbitros) {
        if (id != null && arbitros != null) {
            for (Filial arbitro : arbitros) {
                if (Objects.equals(arbitro.getIdfilial(), id)) {
                    return arbitro;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object beer) {
        if (beer != null) {
            return ((Filial) beer).getIdfilial().toString();
        } else {
            return null;
        }
    }

}
