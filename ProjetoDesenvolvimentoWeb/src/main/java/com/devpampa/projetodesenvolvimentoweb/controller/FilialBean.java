/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.dao.DaoEmpresaHasUsuario;
import com.devpampa.projetodesenvolvimentoweb.dao.DaoFilial;
import com.devpampa.projetodesenvolvimentoweb.dao.DaoUsuario;
import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import com.devpampa.projetodesenvolvimentoweb.model.EmpresaHasUsuario;
import com.devpampa.projetodesenvolvimentoweb.model.EmpresaHasUsuarioPK;
import com.devpampa.projetodesenvolvimentoweb.model.Filial;
import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 * bean serve para trabalhar com as views
 *
 * @author petersonrodrigues
 */
@Named(value = "filialBean") //define como a view vai poder usar ela, chamando usuarioBean.
@ViewScoped //define que essa classe somente ficará viva enquanto o usuário estiver na página que ela esta sendo chamada.
//para durar enquanto sessão usar SessionScoped. 
public class FilialBean implements Serializable {

    @EJB // notação do Enterprise JavaBeans (EJB) que é um componente da plataforma JEE que roda em um container de um servidor de aplicação. https://pt.wikipedia.org/wiki/Enterprise_JavaBeans
    DaoFilial daoFilial; // Injeção da classe daoUsuario no container. Como se estivesse dando um "new" nessa instância
    private Filial filialNovo;

    private boolean novo, detalhe, editar;

    public FilialBean() {
        System.out.println("Construtor sendo executado");
        filialNovo = (Filial) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("filialselecionado");
        if (filialNovo == null) {
            novo = true;
            detalhe = false;
            editar = false;
            filialNovo = new Filial();
        } else {
            if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalhefilial") != null) {
                detalhe = (boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalhefilial");
            } else if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editarfilial") != null) {
                editar = (boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editarfilial");
            }
        }
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("filialselecionado");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("detalhefilial");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("editarfilial");
    }

    public void salvarFilial() {
        System.out.println("Entrou no salvar filial");

        if (filialNovo != null) {
            if (daoFilial.criarFilial(filialNovo) != null) {
                filialNovo = new Filial();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Filial Salva com Sucesso!"));

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Problemas ao Salvar"));

            }
        } else {
            System.out.println("Problemas ao cadastrar");
        }
    }

    public String identificarPerfil(Filial filialSelecionada) {
        if (filialSelecionada != null) {
            if (null == filialSelecionada.getStatus()) {
                return "Ativa";
            } else {
                switch (filialSelecionada.getStatus()) {
                    case 1:
                        return "Ativa";
                    case 2:
                        return "Inativa";
                    default:
                        return "Ativa";
                }
            }
        }
        return "-";
    }

    public DaoFilial getDaoFilial() {
        return daoFilial;
    }

    public void setDaoFilial(DaoFilial daoFilial) {
        this.daoFilial = daoFilial;
    }

    public Filial getFilialNovo() {
        return filialNovo;
    }

    public void setFilialNovo(Filial filialNovo) {
        this.filialNovo = filialNovo;
    }

    public boolean isNovo() {
        return novo;
    }

    public void setNovo(boolean novo) {
        this.novo = novo;
    }

    public boolean isDetalhe() {
        return detalhe;
    }

    public void setDetalhe(boolean detalhe) {
        this.detalhe = detalhe;
    }

    public boolean isEditar() {
        return editar;
    }

    public void setEditar(boolean editar) {
        this.editar = editar;
    }

}
