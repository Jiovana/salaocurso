/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.dao.DaoEmpresa;
import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "empresasBean") //define como a view vai poder usar ela, chamando usuarioBean.
@ViewScoped //define que essa classe somente ficará viva enquanto o usuário estiver na página que ela esta sendo chamada.
//para durar enquanto sessão usar SessionScoped. 
public class EmpresasBean implements Serializable {
    //usar uma ejb para cada DAO
    @EJB // notação do Enterprise JavaBeans (EJB) que é um componente da plataforma JEE que roda em um container de um servidor de aplicação. https://pt.wikipedia.org/wiki/Enterprise_JavaBeans
    DaoEmpresa daoEmpresa; // Injeção da classe daoUsuario no container. Como se estivesse dando um "new" nessa instância
    
    private List<Empresa> listaEmpresa; //lista para pegar todos usuarios do banco

    public EmpresasBean() {
        System.out.println("Construtor sendo executado");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Lista de Empresa"));

    }

    @PostConstruct
    public void init() {
        System.out.println("Init sendo executado");
        listaEmpresa = daoEmpresa.buscarEmpresaNovo();
    }


    public List<Empresa> getListaEmpresa() {
        return listaEmpresa;
    }

    public void setListaEmpresa(List<Empresa> listaEmpresa) {
        this.listaEmpresa = listaEmpresa;
    }
    
     public String identificarPerfil(Empresa empresaSelecinada) {
        if (empresaSelecinada != null) {
            if (null == empresaSelecinada.getStatus()) {
                return "Ativa";
            } else {
                switch (empresaSelecinada.getStatus()) {
                    case 1:
                        return "Ativa";
                    case 2:
                        return "Inativa";
                    default:
                        return "Ativa";
                }
            }
        }
        return "-";
    }
    
    public void verDetalhes(Empresa empresaSelecionada){
        System.out.println("Selecionou verDetalhes");
        // Aqui vcs pode enviar o objeto usuário para outra tela captar. Esse usuário será adicionado a session através do comando:
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("empresaselecionada", empresaSelecionada); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("detalheEmpresa.xhtml"); // redirecionado para uma página
        } catch (IOException ex) {
            Logger.getLogger(EmpresasBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void excluirEmpresa(Empresa empresaSelecionada){
        System.out.println("selecionou para excluir empresa");
      
        try{
         daoEmpresa.excluirEmpresa(empresaSelecionada.getIdempresa());
         System.out.println("Empresa excluida");
        }catch(Exception ex){
             Logger.getLogger(EmpresasBean.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
             listaEmpresa= daoEmpresa.buscarEmpresaNovo();
        }
    }
    
    public void editarEmpresa(Empresa empresaSelecionada){
       if(empresaSelecionada!=null){  
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("empresaselecionada", empresaSelecionada); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("editar_empresa", true); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("detalhes_empresa", false); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("nova_empresa", false); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
        
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("formularioEmpresa.xhtml"); // redirecionado para uma página
        } catch (IOException ex) {
            Logger.getLogger(EmpresasBean.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
    }

   
}
