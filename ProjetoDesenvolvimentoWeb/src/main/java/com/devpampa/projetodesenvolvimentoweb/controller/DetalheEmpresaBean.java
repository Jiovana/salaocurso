/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "detalheEmpresaBean")
@ViewScoped
public class DetalheEmpresaBean implements Serializable {

    private Empresa empresaDetalhes;

    /**
     * Creates a new instance of DetalheUsuarioBean
     */
    public DetalheEmpresaBean() {

    }

    @PostConstruct
    public void init() {
        empresaDetalhes = (Empresa) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("empresaselecionada"); //recupera usuario do escopo
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("empresaselecionada"); // remove usuario do escopo
    }

    public Empresa getEmpresaDetalhes() {
        return empresaDetalhes;
    }

    public void setEmpresaDetalhes(Empresa empresaDetalhes) {
        this.empresaDetalhes = empresaDetalhes;
    }



}
