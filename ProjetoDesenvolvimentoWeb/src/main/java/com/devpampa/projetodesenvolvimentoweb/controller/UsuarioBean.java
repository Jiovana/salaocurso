/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.dao.DaoEmpresaHasUsuario;
import com.devpampa.projetodesenvolvimentoweb.dao.DaoUsuario;
import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import com.devpampa.projetodesenvolvimentoweb.model.EmpresaHasUsuario;
import com.devpampa.projetodesenvolvimentoweb.model.EmpresaHasUsuarioPK;
import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 * bean serve para trabalhar com as views
 *
 * @author petersonrodrigues
 */
@Named(value = "usuarioBean") //define como a view vai poder usar ela, chamando usuarioBean.
@ViewScoped //define que essa classe somente ficará viva enquanto o usuário estiver na página que ela esta sendo chamada.
//para durar enquanto sessão usar SessionScoped. 
public class UsuarioBean implements Serializable {

    @EJB // notação do Enterprise JavaBeans (EJB) que é um componente da plataforma JEE que roda em um container de um servidor de aplicação. https://pt.wikipedia.org/wiki/Enterprise_JavaBeans
    DaoUsuario daoUsuario; // Injeção da classe daoUsuario no container. Como se estivesse dando um "new" nessa instância
    private Usuario usuarioNovo;
    private Empresa empresaSelecionada;
    @EJB
    DaoEmpresaHasUsuario daoEmpresaHasUsuario;

    private boolean novo, detalheUsuario, editarUsuario;

    public UsuarioBean() {
        System.out.println("Construtor sendo executado");
        usuarioNovo = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("usuarioselecionado");
    }

    @PostConstruct
    public void init() {
        System.out.println("Init Sendo Executado");

        if (usuarioNovo == null) {
            novo = true;
            detalheUsuario = false;
            editarUsuario = false;
            usuarioNovo = new Usuario();
        } else {
            usuarioNovo = daoUsuario.buscarUsuarioPorId(usuarioNovo.getIdusuario()); //para ter certeza q esta com usuario certo, caso esteva tentado fazer 2 operações em telas diferentes sobre o mesmo usuario
            if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalheusuario") != null) {
                detalheUsuario = (Boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalheusuario");
                empresaSelecionada = daoEmpresaHasUsuario.buscarEmpresaUsuario(usuarioNovo.getIdusuario());

            } else if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editarusuario") != null) {
                editarUsuario = (Boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editarusuario");
                empresaSelecionada = daoEmpresaHasUsuario.buscarEmpresaUsuario(usuarioNovo.getIdusuario());
            }
        }
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("usuarioselecionado");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("detalheusuario");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("editarusuario");
    }

    public void salvarUsuario() {
        System.out.println("Entrou no salvar usuario");
        usuarioNovo = daoUsuario.criarUsuario(usuarioNovo);
        if (usuarioNovo != null) {
            EmpresaHasUsuarioPK pk = new EmpresaHasUsuarioPK();
            pk.setEmpresaIdempresa(empresaSelecionada.getIdempresa());
            pk.setUsuarioIdusuario(usuarioNovo.getIdusuario());

            EmpresaHasUsuario novaRelacao = new EmpresaHasUsuario();
            novaRelacao.setEmpresa(empresaSelecionada);
            novaRelacao.setUsuario(usuarioNovo);
            novaRelacao.setEmpresa_has_empresaPK(pk);

            if (daoEmpresaHasUsuario.update(novaRelacao) != null) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("listaUsuarios.xhtml"); // redirecionado para uma página
                } catch (IOException ex) {
                    Logger.getLogger(UsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Problemas ao Salvar"));

            }
        } else {
            System.out.println("Problemas ao cadastrar");
        }
    }

    public Usuario getUsuarioNovo() {
        return usuarioNovo;
    }

    public void setUsuarioNovo(Usuario usuarioNovo) {
        this.usuarioNovo = usuarioNovo;
    }

    public Empresa getEmpresaSelecionada() {
        return empresaSelecionada;
    }

    public void setEmpresaSelecionada(Empresa empresaSelecionada) {
        this.empresaSelecionada = empresaSelecionada;
    }

    public String identificarPerfil(Usuario usuarioSelecionado) {
        if (usuarioSelecionado != null) {
            if (null == usuarioSelecionado.getPerfil()) {
                return "Barbeiro";
            } else {
                switch (usuarioSelecionado.getPerfil()) {
                    case 1:
                        return "Administrador";
                    case 2:
                        return "Cabelereiro";
                    default:
                        return "Barbeiro";
                }
            }
        }
        return "-";
    }

    public DaoUsuario getDaoUsuario() {
        return daoUsuario;
    }

    public void setDaoUsuario(DaoUsuario daoUsuario) {
        this.daoUsuario = daoUsuario;
    }

    public DaoEmpresaHasUsuario getDaoEmpresaHasUsuario() {
        return daoEmpresaHasUsuario;
    }

    public void setDaoEmpresaHasUsuario(DaoEmpresaHasUsuario daoEmpresaHasUsuario) {
        this.daoEmpresaHasUsuario = daoEmpresaHasUsuario;
    }

    public boolean isNovo() {
        return novo;
    }

    public void setNovo(boolean novo) {
        this.novo = novo;
    }

    public boolean isDetalheUsuario() {
        return detalheUsuario;
    }

    public void setDetalheUsuario(boolean detalheUsuario) {
        this.detalheUsuario = detalheUsuario;
    }

    public boolean isEditarUsuario() {
        return editarUsuario;
    }

    public void setEditarUsuario(boolean editarUsuario) {
        this.editarUsuario = editarUsuario;
    }

    public String logarUsuario() {
        Integer resultado = daoUsuario.validaUsuario(usuarioNovo.getUsuario(), usuarioNovo.getSenha());
        if (resultado != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Logado"));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("usuario_logado", resultado); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
            return "/index.xhtml?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Falha no acesso"));
            return null;
        }

    }


}
