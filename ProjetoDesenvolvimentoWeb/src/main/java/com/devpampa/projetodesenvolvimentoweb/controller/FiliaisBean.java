/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.controller;

import com.devpampa.projetodesenvolvimentoweb.dao.DaoFilial;
import com.devpampa.projetodesenvolvimentoweb.dao.DaoUsuario;
import com.devpampa.projetodesenvolvimentoweb.model.Filial;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author gomes
 */
@Named(value = "filiaisBean")
@Dependent
public class FiliaisBean {

    /**
     * Creates a new instance of FiliaisBean
     */
     @EJB // notação do Enterprise JavaBeans (EJB) que é um componente da plataforma JEE que roda em um container de um servidor de aplicação. https://pt.wikipedia.org/wiki/Enterprise_JavaBeans
    DaoFilial daoFilial; 
    private List<Filial> listaFilial;  
    
    public FiliaisBean() {
       
    }
     @PostConstruct
    public void init() {
        
        listaFilial= daoFilial.buscarFilial();
    }

    public List<Filial> getListaFilial() {
        return listaFilial;
    }

    public void setListaFilial(List<Filial> listaFilial) {
        this.listaFilial = listaFilial;
    }
    
}
