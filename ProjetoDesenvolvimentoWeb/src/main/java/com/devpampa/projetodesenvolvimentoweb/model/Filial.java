/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author petersonrodrigues
 */
@Entity
@Table(name = "filial")
@XmlRootElement
public class Filial implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfilial")
    private Integer idfilial;
    
    @Size(max = 45)
    @Column(name = "endereco")
    private String endereco;
    
    @Column(name = "valor")
    private Integer valor;
    
    @Column(name = "status")
    private Integer status;
    
    @Size(max = 45)
    @Column(name = "descricao")
    private String descricao;
    
    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa")
    @ManyToOne(optional = false)
    private Empresa empresaIdempresa;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filialIdfilial")
    private List<Reserva> reservaList;

    public Filial() {
    }

    public Filial(Integer idfilial, String endereco, Integer valor, Integer status, Empresa empresaIdempresa) {
        this.idfilial = idfilial;
        this.endereco = endereco;
        this.valor = valor;
        this.status = status;
        this.empresaIdempresa = empresaIdempresa;
    }
    
    
    
    public Filial(Integer idfilial) {
        this.idfilial = idfilial;
    }

    public Integer getIdfilial() {
        return idfilial;
    }

    public void setIdfilial(Integer idfilial) {
        this.idfilial = idfilial;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Empresa getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    public void setEmpresaIdempresa(Empresa empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    @XmlTransient
    public List<Reserva> getReservaList() {
        return reservaList;
    }

    public void setReservaList(List<Reserva> reservaList) {
        this.reservaList = reservaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfilial != null ? idfilial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filial)) {
            return false;
        }
        Filial other = (Filial) object;
        if ((this.idfilial == null && other.idfilial != null) || (this.idfilial != null && !this.idfilial.equals(other.idfilial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.devpampa.projetodesenvolvimentoweb.model.Filial[ idfilial=" + idfilial + " ]";
    }
    
}
