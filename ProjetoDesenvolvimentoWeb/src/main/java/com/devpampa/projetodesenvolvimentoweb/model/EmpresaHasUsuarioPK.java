/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author peter
 */
@Embeddable
public class EmpresaHasUsuarioPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "empresa_idempresa")
    private int empresaIdempresa;

    @Basic(optional = false)
    @NotNull
    @Column(name = "usuario_idusuario")
    private int usuarioIdusuario;

    public EmpresaHasUsuarioPK() {
    }

    public EmpresaHasUsuarioPK(int empresaIdempresa, int usuarioIdusuario) {
        this.empresaIdempresa = empresaIdempresa;
        this.usuarioIdusuario = usuarioIdusuario;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.empresaIdempresa;
        hash = 37 * hash + this.usuarioIdusuario;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpresaHasUsuarioPK other = (EmpresaHasUsuarioPK) obj;
        if (this.empresaIdempresa != other.empresaIdempresa) {
            return false;
        }
        if (this.usuarioIdusuario != other.usuarioIdusuario) {
            return false;
        }
        return true;
    }

    /**
     * @return the empresaIdempresa
     */
    public int getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    /**
     * @param empresaIdempresa the empresaIdempresa to set
     */
    public void setEmpresaIdempresa(int empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    /**
     * @return the usuarioIdusuario
     */
    public int getUsuarioIdusuario() {
        return usuarioIdusuario;
    }

    /**
     * @param usuarioIdusuario the usuarioIdusuario to set
     */
    public void setUsuarioIdusuario(int usuarioIdusuario) {
        this.usuarioIdusuario = usuarioIdusuario;
    }

}
