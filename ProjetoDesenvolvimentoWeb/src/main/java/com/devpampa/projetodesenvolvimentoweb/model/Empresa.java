/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author petersonrodrigues
 */
@Entity
@Table(name = "empresa")
@XmlRootElement
public class Empresa implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idempresa")
    private Integer idempresa;
    
    @Size(max = 45)
    @Column(name = "nome")
    private String nome;
    
    @Size(max = 100)
    @Column(name = "descricao")
    private String descricao;
    
    @Column(name = "status")
    private Integer status;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaIdempresa")
    private List<Filial> filialList;

    public Empresa() {
    }
    
    public Empresa(Integer idempresa, String nome, String descricao, Integer status) {
        this.idempresa = idempresa;
        this.nome = nome;
        this.descricao = descricao;
        this.status = status;
    }

    public Empresa(Integer idempresa) {
        this.idempresa = idempresa;
    }

    public Integer getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(Integer idempresa) {
        this.idempresa = idempresa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

   

    @XmlTransient
    public List<Filial> getFilialList() {
        return filialList;
    }

    public void setFilialList(List<Filial> filialList) {
        this.filialList = filialList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idempresa != null ? idempresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.idempresa == null && other.idempresa != null) || (this.idempresa != null && !this.idempresa.equals(other.idempresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.devpampa.projetodesenvolvimentoweb.model.Empresa[ idempresa=" + idempresa + " ]";
    }
    
}
