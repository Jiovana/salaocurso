/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author peter
 */
@Entity
@Table(name = "empresa_has_usuario")
public class EmpresaHasUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private EmpresaHasUsuarioPK empresa_has_empresaPK;
    
  
    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Empresa empresa;
    
    @JoinColumn(name = "usuario_idusuario", referencedColumnName = "idusuario", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usuario usuario;

    public EmpresaHasUsuario() {
    }

    public EmpresaHasUsuario(Empresa empresa, Usuario usuario) {
        this.empresa = empresa;
        this.usuario = usuario;
    }

    public EmpresaHasUsuario(EmpresaHasUsuarioPK empresa_has_empresaPK, Empresa empresa, Usuario usuario) {
        this.empresa_has_empresaPK = empresa_has_empresaPK;
        this.empresa = empresa;
        this.usuario = usuario;
    }

    /**
     * @return the empresa_has_empresaPK
     */
    public EmpresaHasUsuarioPK getEmpresa_has_empresaPK() {
        return empresa_has_empresaPK;
    }

    /**
     * @param empresa_has_empresaPK the empresa_has_empresaPK to set
     */
    public void setEmpresa_has_empresaPK(EmpresaHasUsuarioPK empresa_has_empresaPK) {
        this.empresa_has_empresaPK = empresa_has_empresaPK;
    }

    /**
     * @return the empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

  
}
