package com.devpampa.projetodesenvolvimentoweb.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Pedro
 * @param <T>
 * @param <PK>
 */
public abstract class Dao<T extends Object, PK extends Integer> {

    @PersistenceContext(unitName = "programaCursoPU")
    private EntityManager entityManager;

    public Dao() {

    }

    // <editor-fold defaultstate="collapsed" desc="INSERT and UPDATE"> 
    /**
     * Salva um objeto mapeado no banco de dados
     *
     * @param obj
     * @return boolean se salvou ou não
     */
    public T salvar(T obj) {

        T entity = getEntityManager().merge(obj);
        getEntityManager().flush();
        return entity;

    }

    public T update(T obj) {
        return salvar(obj);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="DELETE"> 
    public boolean excluir(Integer codigo, Class<?> classe) {
        System.out.println("Entrou excluir");
        try {
            Object excluir = getEntityManager().find(classe, codigo);

            Object auxiliar = getEntityManager().merge(excluir);
            getEntityManager().remove(auxiliar);
            Object o = getEntityManager().find(classe, codigo);
            getEntityManager().flush();

            if (o == null) {
                System.out.println("true");
                return true;
            } else {
                System.out.println("false");
                return false;
            }
        } catch (PersistenceException e) {
            System.out.println("entrou exception excluir");
            return false;
        } catch (Exception e) {
            System.out.println("entreou exception geral");
            return false;
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="SEARCH OBJECT"> 
    public Object buscarObjeto(int codigo, Class<?> classe) {
        Object objeto = null;
        Criteria criteria = getCriteria(classe);
        criteria.add(Restrictions.idEq(codigo));
        return getObject(criteria);
    }

    public Object buscarObjeto(Integer codigo, Class<?> classe) {
        Object objeto = null;
        Criteria criteria = getCriteria(classe);
        criteria.add(Restrictions.idEq(codigo));
        return getObject(criteria);
    }
//    METODO PARA BUSCAR QUALQUER OBJETO UNICO EM QUALQUER CLASSE, (PROPRIEDADE CHAVE, VALOR A SER COMPARADO, TABELA)

    public T buscarObjetoCriteria(String propriedade, String valor, Class classe) {
        Session session = (Session) this.getEntityManager().getDelegate();

        return (T) session.createCriteria(classe)
                .add(Restrictions.eq(propriedade, valor))
                .uniqueResult();

    }

    public T buscarObjetoCriteria(String propriedade, Object valor, Class classe) {
        Session session = (Session) this.getEntityManager().getDelegate();

        return (T) session.createCriteria(classe)
                .add(Restrictions.eq(propriedade, valor))
                .uniqueResult();

    }

    public Object buscarObjeto(String sql, Class<?> classe) {
        //Session session = (Session) this.getEntityManager().getDelegate();

        EntityManager manager = getEntityManager();
        Query query = manager.createQuery(sql);
        query.setMaxResults(1);
        Object ob;
        try {
            ob = query.getSingleResult();

            return ob;
        } catch (NoResultException nre) {
            return null;
        } finally {
            manager.flush();
            manager.clear();
        }

    }

    public Object buscarObjetosGeral(HashMap<String, Object> filtros, Class<?> classe) {
        Criteria criteria = getCriteria(classe);
        for (Map.Entry<String, Object> entry : filtros.entrySet()) {
            String campo = entry.getKey();
            Object valor = entry.getValue();
            criteria.add(Restrictions.eq(campo, valor));
        }
        List<T> list = criteria.list();
        return list;
    }

    public Object buscarObjetoGeral(HashMap<String, Object> filtros, Class<?> classe) {
        System.out.println("buscar geral");
        Criteria criteria = getCriteria(classe);
        for (Map.Entry<String, Object> entry : filtros.entrySet()) {
            String campo = entry.getKey();
            Object valor = entry.getValue();
            criteria.add(Restrictions.eq(campo, valor));
        }
        return getObject(criteria);
    }

    public Object buscarObjetoCriteriaINT(String propriedade, int valor, Class<?> classe) {
        Object objeto = null;
        Criteria criteria = getCriteria(classe);
        criteria.add(Restrictions.eq(propriedade, valor));
        return getObject(criteria);
    }

    public List<T> buscarObjetosCritera(String propriedade, Object valor, Class classe) {
        Criteria criteria = getCriteria(classe);
        criteria.add(Restrictions.eq(propriedade, valor));
        List<T> list = criteria.list();
        return list;
    }

    public List<T> buscarObjetos(Class classe) {
        Criteria criteria = getCriteria(classe);
        List<T> list = criteria.list();

        return list;
    }

    private Object getObject(Criteria criteria) {
        Object object = criteria.uniqueResult();
        getEntityManager().clear();
        return object;
    }

    public List<T> buscarObjetosOrdenada(String propriedade, Class<?> classe) {
        Criteria criteria = getCriteria(classe);
        criteria.addOrder(Order.asc(propriedade));
        List<T> list = criteria.list();
        getEntityManager().clear();
        return list;
    }

    public List<T> buscarObjetosSql(String sql, Class<?> classe) {
        EntityManager manager = getEntityManager();
        Query query = manager.createQuery(sql);
        List<T> ob;
        try {
            ob = query.getResultList();

            return ob;
        } catch (NoResultException nre) {
            return null;
        } finally {
            manager.flush();
            manager.clear();
        }
    }
    
    
    // </editor-fold>

    private Criteria getCriteria(Class classe) {

        Session session = (Session) getEntityManager().getDelegate();

        Transaction tx = null;
        Criteria criteria = null;
        try {
            tx = session.getTransaction();//cria uma transação para o hibernate conectar no banco
            criteria = session.createCriteria(classe);
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            getEntityManager().clear();
        }
        return criteria;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
