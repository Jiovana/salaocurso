/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.dao;

import com.devpampa.projetodesenvolvimentoweb.model.Reserva;
import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

/**
 *
 * @author Peterson
 */
@Stateful
/*
Utilizar Stateful quando precisa manter o estado conversacional.
Exemplo: Em um carrinho de compras, onde você adiciona diversos itens e são itens pertinentes somente à um cliente, ou seja, existe a necessidade de manter a lista.
Utilizar Stateless quando não há necessidade de manter o estado dos valores.
Exemplo: Listar os produtos da loja (é a mesma lista para qualquer cliente, certo?)
 */
@LocalBean
public class DaoReserva extends Dao<Reserva, Integer> {

    public DaoReserva() {
        super();
    }


    public Reserva criarReserva(Reserva reserva) {
        return super.salvar(reserva);
    }


    public Reserva updateReserva(Reserva reserva) {
        return super.update(reserva);
    }

 
    public Reserva buscarReservaPorId(Integer idUsuario) {
        return (Reserva) super.buscarObjeto(idUsuario, Reserva.class);
    }


    public List<Reserva> buscarReserva() {
        return (List<Reserva>) super.buscarObjetos(Reserva.class);
    }

    public boolean excluirReserva(Integer idUser) {
        return super.excluir(idUser, Reserva.class);
    }

}
