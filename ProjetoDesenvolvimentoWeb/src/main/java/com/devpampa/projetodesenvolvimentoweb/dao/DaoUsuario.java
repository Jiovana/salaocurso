/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.dao;

import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Peterson
 */
@Stateful
/*
Utilizar Stateful quando precisa manter o estado conversacional.
Exemplo: Em um carrinho de compras, onde você adiciona diversos itens e são itens pertinentes somente à um cliente, ou seja, existe a necessidade de manter a lista.
Utilizar Stateless quando não há necessidade de manter o estado dos valores.
Exemplo: Listar os produtos da loja (é a mesma lista para qualquer cliente, certo?)
 */
@LocalBean
public class DaoUsuario extends Dao<Usuario, Integer> {

    public DaoUsuario() {
        super();
    }

    /**
     * Método para criar novo usuario
     *
     * @param usuario
     * @return
     */
    public Usuario criarUsuario(Usuario usuario) {
        return super.salvar(usuario);
    }

    /**
     * Método para atualizar usuario existente
     *
     * @param usuario
     * @return
     */
    public Usuario updateUsuario(Usuario usuario) {
        return super.update(usuario);
    }

    /**
     * Método para buscar usuario por ID
     *
     * @param idUsuario
     * @return
     */
    public Usuario buscarUsuarioPorId(Integer idUsuario) {
        return (Usuario) super.buscarObjeto(idUsuario, Usuario.class);
    }
    
   
    
     public Integer validaUsuario(String nome, String senha) {
        EntityManager manager = getEntityManager();
        Query query = manager.createQuery("SELECT user FROM Usuario AS user WHERE user.usuario ='"+nome+"'AND user.senha='"+senha+"'", Usuario.class);
        Usuario usuarioBanco;
        try {       
            usuarioBanco = (Usuario) query.getSingleResult();
            if(usuarioBanco == null){
                return null;
            }else
                return usuarioBanco.getIdusuario();
        } catch (NoResultException nre) {
            return null;
        } finally {
            manager.flush();
            manager.clear();
        }
    }

    /**
     * Método para buscar todos usuarios
     *
     * @return
     */
    public List<Usuario> buscarUsuarios() {
        return (List<Usuario>) super.buscarObjetos(Usuario.class);
    }

    /**
     * Método para excluir usuario por ID
     *
     * @param idUser
     * @return
     */
    public boolean excluirUsuario(Integer idUser) {
        return super.excluir(idUser, Usuario.class);
    }

}
