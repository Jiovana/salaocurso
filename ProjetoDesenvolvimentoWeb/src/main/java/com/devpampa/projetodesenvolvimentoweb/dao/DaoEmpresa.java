/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.dao;

import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

/**
 *
 * @author Peterson
 */
@Stateful
/*
Utilizar Stateful quando precisa manter o estado conversacional.
Exemplo: Em um carrinho de compras, onde você adiciona diversos itens e são itens pertinentes somente à um cliente, ou seja, existe a necessidade de manter a lista.
Utilizar Stateless quando não há necessidade de manter o estado dos valores.
Exemplo: Listar os produtos da loja (é a mesma lista para qualquer cliente, certo?)
 */
@LocalBean
public class DaoEmpresa extends Dao<Empresa, Integer> {

    public DaoEmpresa() {
        super();
    }

    /**
     * Método para criar novo empressa
     *
     * @param empresa
     * @return
     */
    public Empresa criarEmpresa(Empresa empresa) {
        return super.salvar(empresa);
    }

    /**
     * Método para atualizar usuario existente
     *
     * @param empresa
     * @return
     */
    public Empresa updateEmpresa(Empresa empresa) {
        return super.update(empresa);
    }

    /**
     * Método para buscar usuario por ID
     *
     * @param idEmpresa
     * @return
     */
    public Empresa buscarUsuarioPorId(Integer idEmpresa) {
        return (Empresa) super.buscarObjeto(idEmpresa, Empresa.class);
    }

    /**
     * Método para buscar todos usuarios
     *
     * @return
     */
    public List<Empresa> buscarEmpresa() {
        return (List<Empresa>) super.buscarObjetos(Empresa.class);
    }

    //busca com sql puro para buscar todos atributos exceto a lista de filiais
    public List<Empresa> buscarEmpresaNovo() {
        System.out.println("Novo buscar empresa");
        return (List<Empresa>) super.buscarObjetosSql("SELECT NEW com.devpampa.projetodesenvolvimentoweb.model.Empresa(idempresa, nome, descricao, status) FROM Empresa", Empresa.class);
    }

    /**
     * Método para excluir usuario por ID
     *
     * @param idEmpresa
     * @return
     */
    public boolean excluirEmpresa(Integer idEmpresa) {
        return super.excluir(idEmpresa, Empresa.class);
    }

}
