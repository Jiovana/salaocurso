/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.dao;

import com.devpampa.projetodesenvolvimentoweb.model.Empresa;
import com.devpampa.projetodesenvolvimentoweb.model.EmpresaHasUsuario;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

/**
 *
 * @author Peterson
 */
@Stateful
/*
Utilizar Stateful quando precisa manter o estado conversacional.
Exemplo: Em um carrinho de compras, onde você adiciona diversos itens e são itens pertinentes somente à um cliente, ou seja, existe a necessidade de manter a lista.
Utilizar Stateless quando não há necessidade de manter o estado dos valores.
Exemplo: Listar os produtos da loja (é a mesma lista para qualquer cliente, certo?)
 */
@LocalBean
public class DaoEmpresaHasUsuario extends Dao<EmpresaHasUsuario, Integer> {

    public DaoEmpresaHasUsuario() {
        super();
    }
    
    public EmpresaHasUsuario updateUsuario(EmpresaHasUsuario relacao) {
        return super.update(relacao);
    }
    
    public Empresa buscarEmpresaUsuario(Integer idEmpresa) {
        System.out.println("veio ID: " + idEmpresa);
        return (Empresa) super.buscarObjeto("SELECT empresaHasUsuario.empresa FROM EmpresaHasUsuario empresaHasUsuario WHERE empresaHasUsuario.usuario.idusuario=" + idEmpresa, EmpresaHasUsuario.class);

    }
    
}
