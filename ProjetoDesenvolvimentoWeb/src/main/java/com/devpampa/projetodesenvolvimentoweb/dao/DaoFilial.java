/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devpampa.projetodesenvolvimentoweb.dao;

import com.devpampa.projetodesenvolvimentoweb.model.Filial;
import com.devpampa.projetodesenvolvimentoweb.model.Usuario;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

/**
 *
 * @author Peterson
 */
@Stateful
/*
Utilizar Stateful quando precisa manter o estado conversacional.
Exemplo: Em um carrinho de compras, onde você adiciona diversos itens e são itens pertinentes somente à um cliente, ou seja, existe a necessidade de manter a lista.
Utilizar Stateless quando não há necessidade de manter o estado dos valores.
Exemplo: Listar os produtos da loja (é a mesma lista para qualquer cliente, certo?)
 */
@LocalBean
public class DaoFilial extends Dao<Filial, Integer> {

    public DaoFilial() {
        super();
    }

    /**
     * Método para criar novo usuario
     *
     * @param Filial
     * @return
     */
    public Filial criarFilial(Filial filial) {
        return super.salvar(filial);
    }


    public Filial updateFilial(Filial filial) {
        return super.update(filial);
    }


    public Filial buscarFilialPorId(Integer idFilial) {
        return (Filial) super.buscarObjeto(idFilial, Filial.class);
    }

    /**
     * Método para buscar todos usuarios
     *
     * @return
     */
    public List<Filial> buscarFilial() {
        return (List<Filial>) super.buscarObjetos(Filial.class);
    }

    /**
     * Método para excluir usuario por ID
     *
     * @param idUser
     * @return
     */
    public boolean excluirFilial(Integer idUser) {
        return super.excluir(idUser, Filial.class);
    }

}
